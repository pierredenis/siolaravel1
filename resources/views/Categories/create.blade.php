<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="/posts" method="POST">
    @csrf
    <label for="title">title</label>
    <input type="text" name="title" placeholder="title">
    <label for="content">content</label>
    <input type="text" name="content" placeholder="content">
    <input type="submit">
</form>

</body>
</html>
