<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">

            <h1>Création d'un utilisateur</h1>

            <form action="" method="POST">
                @csrf
                <label for="">Email</label>
                <input type="email" name="email">
                <label for="">Password</label>
                <input type="text" name="password">
                <label for="">Name</label>
                <input type="text" name="name">
                <input type="submit" class="btn btn-success">
            </form>
        </div>
    </div>
</div>
</body>
</html>
