<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return view('Users/index',['users' => $users]);
    }
    public function show($id){
        $user = User::findOrFail($id);
        return view('Users/show',['user' => $user]);
    }
    public function create(){
        return view('Users/create');
    }
    public function save(Request $request){
        //premiere méthode par instanciation d'objet
        $user = new User();
        $user->name = $request->name;
        $user->password = $request->password;
        $user->email = $request->email;
        $user->save();

        //Deuxième méthode (mass assignement)
        //ATTENTION IL FAUT RENSEIGNER DANS LE MODEL
        //UN ATTRIBUT protected $fillable = [] Le tableau doit être composé des champs que vous souhaitez enregistrer
        User::create($request->all());
    }
}
